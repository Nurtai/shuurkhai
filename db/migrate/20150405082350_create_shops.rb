class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.string :logo
      t.string :address
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
  end
end
