class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :location
      t.string :postcode
      t.string :region_name
      t.integer :parent_id
      t.string :border_color
      t.string :bg_color
      t.string :center
      t.text :coordinate
      t.string :filename

      t.timestamps null: false
    end
  end
end
