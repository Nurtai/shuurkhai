class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :cname
      t.integer :parent_id
      t.string :center
      t.string :filename

      t.timestamps null: false
    end
  end
end
