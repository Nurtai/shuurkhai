# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150405085828) do

  create_table "countries", force: :cascade do |t|
    t.string   "cname",      limit: 255
    t.integer  "parent_id",  limit: 4
    t.string   "center",     limit: 255
    t.string   "filename",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "country", force: :cascade do |t|
    t.string  "cname",     limit: 200, null: false
    t.integer "parent_id", limit: 4
    t.string  "center",    limit: 200
    t.string  "filename",  limit: 100
  end

  create_table "locations", force: :cascade do |t|
    t.string   "location",     limit: 255
    t.string   "postcode",     limit: 255
    t.string   "region_name",  limit: 255
    t.integer  "parent_id",    limit: 4
    t.string   "border_color", limit: 255
    t.string   "bg_color",     limit: 255
    t.string   "center",       limit: 255
    t.text     "coordinate",   limit: 65535
    t.string   "filename",     limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "logo",       limit: 255
    t.string   "address",    limit: 255
    t.string   "email",      limit: 255
    t.string   "phone",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "latlng",     limit: 255
  end

end
