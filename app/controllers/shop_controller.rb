class ShopController < ApplicationController
	def new
		@shop = Shop.new
		@shops = Shop.all
	end
	def create
		 @shop = Shop.new(params[:shop].permit(:name, :address, :email, :phone, :latlng))
		 	uploaded_io = params[:shop][:logo]
		 	filename = SecureRandom.hex + "-" + uploaded_io.original_filename
            File.open(Rails.root.join('public', 'uploads', filename), 'wb') do |file|
            	file.write(uploaded_io.read)
        	end
        	@shop.logo = filename
            if @shop.save
                    redirect_to shop_path
            else
                    render 'new'
            end
	end
	
end
