class Country < ActiveRecord::Base
	has_many :children, class_name: "Country", foreign_key: "parent_id"
	belongs_to :parent, class_name: "Country"

	def self.search(query)
  		where("cname like ?", "%#{query}%") 
	end
end
